const fs = require('fs');
const log4js = require('log4js')
const date = require('date-and-time');

const logger = (folder, message) => {
    const now = new Date();
    console.log(`${date.format(now, 'YYYY-MM-DD HH:mm:SS')} : [${folder}] = ${JSON.stringify(message)}`);

    if (!log4js.isConfigured()) {
        log4js.configure({
            appenders: {
                acd_log: { 
                    type: "file", 
                    filename: process.env.LOG_NAME, 
                    maxLogSize: process.env.LOG_MAX_SIZE, 
                    backups: process.env.LOG_BACKUP_NUMBER, 
                }
            },
            categories: { 
                default: { appenders: ['acd_log'], level: 'debug' } 
            },
        });
    }
    const loggers = log4js.getLogger('acd_log');
    loggers.debug(`[${folder}] = ${JSON.stringify(message)}`);
}

const logger_old = (folder, message) => {
    const now = new Date();
    const directory = `./logs/${date.format(now, 'YYYY-MM-DD')}/${folder}`;
    const filename = date.format(now, 'YYYYMMDD');

    try {
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }

        // fs.writeFile(`${directory}/${filename}.txt`, String(message), function (err) {
        //     if (err) return console.log(err);
        // });
        fs.appendFile(`${directory}/${filename}.txt`, `${date.format(now, 'YYYY-MM-DD HH:mm:SS')} : ${String(message)}\n\n`, function (err) {
            if (err) return console.log(err);
        });
    } catch (err) {
        console.error(err)
    }
}
// logger();

module.exports = logger;
