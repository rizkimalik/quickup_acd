'use strict'
const dotenv = require('dotenv');
const logger = require('./helper/logger');
dotenv.config();

const { blending_chat, blending_email, blending_whatsapp, blending_twitter, blending_facebook, blending_instagram, blending_telegram } = require('./app/blending');
const starting_msg = `App Blending is running.. ✅ 🚀🚀🚀`;
logger('acd/start', starting_msg);

// blending_chat();
blending_email();
blending_whatsapp();
blending_telegram();
blending_facebook();
blending_instagram();
// blending_twitter();

// setInterval(blending_chat, 3 * 1000);
setInterval(blending_email, 3 * 1000);
setInterval(blending_whatsapp, 3 * 1000);
setInterval(blending_telegram, 3 * 1000);
setInterval(blending_facebook, 3 * 1000);
setInterval(blending_instagram, 3 * 1000);
// setInterval(blending_twitter, 3 * 1000);


